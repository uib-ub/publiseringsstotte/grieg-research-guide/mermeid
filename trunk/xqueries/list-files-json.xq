xquery version "1.0" encoding "UTF-8";

import module namespace loop="http://kb.dk/this/getlist" at "./main_loop.xqm";
import module namespace  app="http://kb.dk/this/listapp" at "./list_utils.xqm";

declare namespace xl="http://www.w3.org/1999/xlink";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace response="http://exist-db.org/xquery/response";
declare namespace fn="http://www.w3.org/2005/xpath-functions";
declare namespace file="http://exist-db.org/xquery/file";
declare namespace util="http://exist-db.org/xquery/util";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace ft="http://exist-db.org/xquery/lucene";
declare namespace ht="http://exist-db.org/xquery/httpclient";

declare namespace local="http://kb.dk/this/app";
declare namespace m="http://www.music-encoding.org/ns/mei";

declare option exist:serialize "method=json media-type=text/javascript";

declare variable $genre  := request:get-parameter("genre","") cast as xs:string;
declare variable $coll   := request:get-parameter("c",    "") cast as xs:string;
declare variable $query  := request:get-parameter("query","") cast as xs:string;
declare variable $page   := request:get-parameter("page", "1") cast as xs:integer;
declare variable $number := request:get-parameter("itemsPerPage","20") cast as xs:integer;

declare variable $database := "/db/dcm";

declare variable $from     := ($page - 1) * $number + 1;
declare variable $to       :=  $from      + $number - 1;

declare variable $published_only := 
request:get-parameter("published_only","") cast as xs:string;

declare function local:format-json(
  $doc as node(),
  $pos as xs:integer ) as node() {
    
    let $document-json :=
    
    <document>
        <name>{util:document-name($doc)}</name>
        <title>{$doc/ancestor-or-self::m:mei/m:meiHead[1]/m:fileDesc[1]/m:titleStmt[1]/m:title[1]/text()}</title>
        <collection>{$doc/ancestor-or-self::m:mei/m:meiHead[1]/m:fileDesc[1]/m:seriesStmt[1]/m:identifier[@type='file_collection'][1]/text()}</collection>
   </document>

	
	return $document-json
  };

    
      let $list := loop:getlist($database,$published_only,$coll,$genre,$query)
      return      
      <root>
       { 
       for $doc at $count in $list[position() = ($from to $to)]
            return local:format-json($doc,$count)
            }
      </root>
          
          